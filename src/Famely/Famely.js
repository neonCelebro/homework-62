import React, { PureComponent } from 'react';

import './Famely.css';
import frame1 from '../img/frame1.png';
import frame2 from '../img/frame2.png';
import frame3 from '../img/frame3.png';
import gf from '../img/gf.jpg';
import me from '../img/me.jpg';
import mth from '../img/mth.jpg';


class Famely extends PureComponent {
  getToGrandfather = () => {
    this.props.history.push('/grandfather');
  };
  getToMother = () => {
    this.props.history.push('/mother');
  };
  getToMe = () => {
    this.props.history.push('/me');
  };
  render(){
    return(
        <div className='mainFrame'>
          <h1 className='mainText'>Моя семья</h1>
          <div className='grandfather'>
            <img onClick={()=>this.getToGrandfather()} className='frame frame1' alt='#' src={frame1}/>
            <img className='gf' alt='#' src={gf}/>
          </div>
          <div className='isMe'>
            <img onClick={()=>this.getToMe()} className='frame frame2' alt='#' src={frame2}/>
            <img className='me' alt='#' src={me}/>
          </div>
          <div className='mother'>
            <img onClick={()=>this.getToMother()} className='frame frame3' alt='#' src={frame3}/>
            <img className='mth' alt='#' src={mth}/>
          </div>
      </div>
    );
  }
};

export default Famely;
