import React, { PureComponent } from 'react';

import './IsMe.css';
import frame2 from '../img/frame2.png';
import me from '../img/me.jpg';


class IsMe extends PureComponent {

  goBack = () => {
    this.props.history.push('/');
  }

  render(){
    return(
        <div className='mainFrame'>
          <h1 className='textMe'>Собственно Я</h1>
          <div className='IsMe'>
            <img className='frameMe' alt='#' src={frame2}/>
            <img className='photoMe' alt='#' src={me}/>
          </div>
          <p onClick={()=> this.goBack()} className='textBack'>назад...</p>
        </div>
    );
  }
};

export default IsMe;
