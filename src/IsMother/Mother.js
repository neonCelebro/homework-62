import React, { PureComponent } from 'react';

import './Mother.css';
import frame3 from '../img/frame3.png';
import mth from '../img/mth.jpg';


class Mother extends PureComponent {

  goBack = () => {
    this.props.history.push('/');
  }

  render(){
    return(
      <div className='mainFrame'>
        <h1 className='textMother'>Лариса Анатольевна</h1>
          <div className='Mother'>
            <img className='frameMother' alt='#' src={frame3}/>
            <img className='photoMother' alt='#' src={mth}/>
          </div>
          <p onClick={()=> this.goBack()} className='textBack'>назад...</p>
        </div>
    );
  }
};

export default Mother;
