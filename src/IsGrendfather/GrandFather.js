import React, { PureComponent } from 'react';

import './GrandFather.css';
import frame1 from '../img/frame1.png';
import gf from '../img/gf.jpg';


class GrandFather extends PureComponent {

  goBack = () => {
    this.props.history.push('/');
  }

  render(){
    return(
      <div className='mainFrame'>
        <h1 className='text'>Анатолий Владимирович</h1>
          <div className='grandfather'>
            <img className='frameGrandfather' alt='#' src={frame1}/>
            <img className='photoGrandfather' alt='#' src={gf}/>
          </div>
          <p onClick={()=> this.goBack()} className='textBack'>назад...</p>
        </div>
    );
  }
};

export default GrandFather;
