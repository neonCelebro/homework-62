import React, { Component, Fragment } from 'react';
import {Switch, Route} from 'react-router-dom';

import Famely from './Famely/Famely';
import GrandFather from './IsGrendfather/GrandFather';
import IsMe from './IsMe/IsMe';
import Mother from './IsMother/Mother';
import Background from './Background/Background';

import './App.css';

class App extends Component {

  render() {
    return (
      <Fragment>
        <Background/>
        <Switch>
          <Route path='/' exact component={Famely} />
          <Route path='/grandfather' exact component={GrandFather}/>
          <Route path='/me' exact component={IsMe} />
          <Route path='/mother' exact component={Mother} />
        </Switch>
      </Fragment>
    );
  }
}

export default App;
