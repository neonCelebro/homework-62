import React, { PureComponent } from 'react';
import './Background.css';
import curtains from '../img/curtains.jpg';

class Background extends PureComponent {
  render(){
    return(
      <div className='background'>
        <img className='curtains' alt='#' src={curtains}/>
      </div>
    );
  }
};

export default Background;
